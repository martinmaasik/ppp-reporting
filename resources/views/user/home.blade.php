@extends('layouts.app')

@section('content')
@include('alerts/success')
<div class="container">
      <div class="panel panel-default" style="position:fixed; right:0; left:0; margin-left:auto; margin-right:auto; width:60%; max-width:350px; min-width:250px">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                  <form>
                    <div class="row">
                      <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('PPP.submit.get') }}'">Submit a new PPP report</button></div>
                    </div>
                    <div class="row top-buffer">
                      <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('PPP.view.get') }}'">View submitted reports</button></div>
                    </div>
                    <div class="row top-buffer">
                      <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('PPP.edit.get') }}'">Edit submitted reports</button></div>
                    </div>
                    <div class="row top-buffer">
                      <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('overview.get') }}'">Overview</button></div>
                    </div>
                  </form>
                </div>
    </div>
</div>
@endsection
