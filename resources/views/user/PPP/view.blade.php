@extends('layouts.app')
@section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default" style="min-width: 415px">
                  <div class="panel-heading">View submitted reports</div>
                  <div class="panel-body">
                    PPP report | <b>{{ $selectedViewableReport->period }}</b> | submitted by <b>{{ $selectedViewableReport->user }}</b><br>
                    <small><i>submitted {{ $selectedViewableReport->created_at }} / last edit {{ $selectedViewableReport->updated_at }}</i></small><br><br>
                    <table style="table-layout:fixed; width:100%" frame="void">
                      <tr><td style="width:140px">Emails to new leads:</td>
                      <td>{{ $selectedViewableReport->emails }}</td></tr>
                      <tr><td>Calls to new leads:</td>
                      <td>{{ $selectedViewableReport->calls }}</td></tr>
                      <tr><td>Demos done:</td>
                      <td>{{ $selectedViewableReport->demos }}</td></tr>
                      <tr><td>Trials started:</td>
                      <td>{{ $selectedViewableReport->trials }}</td></tr>
                      <tr><td>Deals closed:</td>
                      <td>{{ $selectedViewableReport->deals }}</td></tr>
                      <tr><td style="vertical-align:top">Notes:</td>
                        @if ($selectedViewableReport->notes != '')
                        <td><div style="white-space:pre-wrap; word-wrap: break-word; min-width:200px">{{ $selectedViewableReport->notes }}</div></td></tr>
                        @else
                          <td>none submitted</td></tr>
                        @endif
                      <tr><td style="vertical-align:top">Problems:</td>
                        @if ($selectedViewableReport->problems != '')
                          <td><div style="white-space:pre-wrap; word-wrap: break-word">{{ $selectedViewableReport->problems }}</div></td></tr>
                        @else
                          <td>none submitted</td></tr>
                        @endif
                      <tr><td style="vertical-align:top">Plans:</td>
                        @if ($selectedViewableReport->next_week != '')
                          <td><div style="white-space:pre-wrap; word-wrap: break-word">{{ $selectedViewableReport->next_week }}</div></td></tr>
                        @else
                          <td>none submitted</td></tr>
                        @endif
                    </table>

                    <br>
                    @include('redirect-buttons/back')
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
