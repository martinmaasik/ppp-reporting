@extends('layouts.app')

@section('content')

@include('alerts/success')
<div class="container-fluid">
    <div class="panel panel-default" style="position:fixed; right:0; left:0; margin-left:auto; margin-right:auto; width:60%; max-width:600px; min-width:300px">
      <div class="panel-heading">Dashboard</div>
        <div class="panel-body">
              <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel panel-default" style="margin-left:auto; margin-right:auto; max-width:260px">
                          <div class="panel-heading">Employees</div>
                          <div class="panel-body">
                              <div class="row">
                                <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('create.get') }}'">Create a new user</button></div>
                              </div>
                              <div class="row top-buffer">
                                <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('edit.select.get') }}'">Edit or delete users</button></div>
                              </div>
                              <div class="row top-buffer">
                                <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('overview.get') }}'">PPP overview</button></div>
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default" style="margin-left:auto; margin-right:auto; max-width:260px">
                          <div class="panel-heading">Clients & Contracts</div>
                          <div class="panel-body">
                              <div class="row">
                                <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('companies.menu.get') }}'">Company profiles</button></div>
                              </div>
                              <div class="row top-buffer">
                                <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('people.menu.get') }}'">Person profiles</button></div>
                              </div>
                              <div class="row top-buffer">
                                <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('contracts.menu.get') }}'">Contracts</button></div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>
</div>
@endsection
