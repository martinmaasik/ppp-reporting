@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">E-mail configuration settings</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{ route('config.post') }}">
                    {{ csrf_field() }}

                      <div class="form-group">
                          <label for="name" class="col-md-4 control-label">Username</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="username" value="{{ $old['username'] }}" placeholder="{{ $default->username }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="password" class="col-md-4 control-label">Password</label>
                          <div class="col-md-6">
                              <input type="password" class="form-control" name="password" value="">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="from_name" class="col-md-4 control-label">From name</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="from_name" value="" placeholder="Not required">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="from_address" class="col-md-4 control-label">From address</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="from_address" value="{{ $old['from']['address'] }}" placeholder="{{ $default->from_address }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="driver" class="col-md-4 control-label">Driver</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="driver" value="{{ $old['driver'] }}" placeholder="{{ $default->driver }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="host" class="col-md-4 control-label">Host</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="host" value="{{ $old['host'] }}" placeholder="{{ $default->host }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="port" class="col-md-4 control-label">Port</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="port" value="{{ $old['port'] }}" placeholder="{{ $default->port }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="encryption" class="col-md-4 control-label">Encryption</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="encryption" value="{{ $old['encryption'] }}" placeholder="{{ $default->encryption }}">
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              @include('redirect-buttons/back')
                              <button type="submit" class="btn btn-primary">
                                  Submit settings
                              </button>
                          </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
