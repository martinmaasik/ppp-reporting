Greetings {{ $adminName }},<br><br>
PPP report for period <b>{{ $newReportData['period'] }}</b> submitted by <b>{{ $newReportData['user'] }}</b>:
<br><br><div class="panel-body">
  <table style="table-layout:fixed; width:100%" >
    <tr><td style="width:65px">Emails:</td>
    <td>{{ $newReportData['emails'] }}</td></tr>
    <tr><td style="width:65px">Calls:</td>
    <td>{{ $newReportData['calls'] }}</td></tr>
    <tr><td style="width:65px">Demos:</td>
    <td>{{ $newReportData['demos'] }}</td></tr>
    <tr><td style="width:65px">Trials:</td>
    <td>{{ $newReportData['trials'] }}</td></tr>
    <tr><td style="width:65px">Deals:</td>
    <td>{{ $newReportData['deals'] }}</td></tr>
    <tr><td style="width:65px; vertical-align:top">Notes:</td>
      @if ($newReportData['notes'] != NULL)
      <td><div style="white-space:pre-wrap; word-wrap: break-word">{{ $newReportData['notes'] }}</div></td></tr>
      @else
        <td>none submitted</td></tr>
      @endif
    <tr><td style="width:65px; vertical-align:top">Problems:</td>
      @if ($newReportData['problems'] != NULL)
        <td><div style="white-space:pre-wrap; word-wrap: break-word">{{ $newReportData['problems'] }}</div></td></tr>
      @else
        <td>none submitted</td></tr>
      @endif
    <tr><td style="width:65px; vertical-align:top">Problems:</td>
      @if ($newReportData['plans'] != NULL)
        <td><div style="white-space:pre-wrap; word-wrap: break-word">{{ $newReportData['plans'] }}</div></td></tr>
      @else
        <td>none submitted</td></tr>
      @endif
  </table>
