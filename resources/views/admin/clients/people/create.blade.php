@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create a new person profile</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{ route('people.create.post') }}">
                    {{ csrf_field() }}

                      <div class="form-group">
                          <label for="name" class="col-md-4 control-label">Name</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="name">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="address" class="col-md-4 control-label">Address</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="address">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="personal_code" class="col-md-4 control-label">Personal code</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="personal_code">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="passport_nr" class="col-md-4 control-label">Passport number</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="passport_nr">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="passport_place" class="col-md-4 control-label">Passport place of issue</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="passport_place">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="bank" class="col-md-4 control-label">Bank</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="bank">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="swift" class="col-md-4 control-label">SWIFT</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="swift">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="iban" class="col-md-4 control-label">IBAN</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="iban">
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              @include('redirect-buttons/back')
                              <button type="submit" class="btn btn-primary">
                                  Create profile
                              </button>
                          </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
