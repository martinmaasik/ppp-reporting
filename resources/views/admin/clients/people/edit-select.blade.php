@extends('layouts.app')
@section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">Edit or delete person profiles</div>
                  <div class="panel-body">
                        @if (empty($people))
                          There are currently no active person records in the system.<br><br>
                          @include('redirect-buttons/back')
                          <button type="button" class="btn btn-primary" onclick="location.href='{{ route('people.create.get') }}'">Go create profile</button>
                        @else
                          <form role="form" method="POST" action="{{ route('people.edit.select.post') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                            Select person:<br>
                              <select id="soflow-color" name="name">
                                @foreach ($people as $person)
                                    <option value="{{ $person }}">{{ $person }}</option>
                                @endforeach
                              </select>
                            </div>
                            @include('redirect-buttons/back')
                            <input type="submit" name="button" value="Edit" class="btn btn-primary">
                            <input type="submit" onclick="return confirm('Are you sure you want to delete the selected person record?');" name="button" value="Delete" class="btn btn-danger">
                          </form>
                        @endif
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
