@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create a new company profile</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{ route('companies.create.post') }}">
                    {{ csrf_field() }}

                      <div class="form-group">
                          <label for="name" class="col-md-4 control-label">Name</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="name">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="reg_nr" class="col-md-4 control-label">Registration number</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="reg_nr">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="address" class="col-md-4 control-label">Address</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="address">
                          </div>
                      </div>


                      <div class="form-group">
                          <label for="representative" class="col-md-4 control-label">Representative role</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="representative" placeholder="e.g CEO, board member, ...">
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              @include('redirect-buttons/back')
                              <button type="submit" class="btn btn-primary">
                                  Create profile
                              </button>
                          </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
