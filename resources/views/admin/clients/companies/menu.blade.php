@extends('layouts.app')

@section('content')
<div class="container-fluid">
  @include('alerts/success')
    <div class="panel panel-default" style="position:fixed; right:0; left:0; margin-left:auto; margin-right:auto; width:20%; max-width:300px; min-width:200px">
        <div class="panel-heading">Company profiles</div>
        <div class="panel-body">
          <form>
            <div class="row">
              <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('companies.create.get') }}'">Create a new profile</button></div>
            </div>
            <div class="row top-buffer">
              <div class="col-sm-12"><button type="button" class="btn btn-default" style="width:100%" onclick="window.location='{{ URL::route('companies.edit.select.get') }}'">Edit or delete profiles</button></div>
            </div>
          </form>
          <br>
          @include('redirect-buttons/back')
        </div>
    </div>
</div>
@endsection
