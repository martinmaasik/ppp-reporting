@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit company profile</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{ route('companies.edit.post') }}">
                    {{ csrf_field() }}

                      <div class="form-group">
                          <label for="name" class="col-md-4 control-label">Name</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="name" value="{{ $selectedCompany->name }}" placeholder="{{ $selectedCompany->name }}">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="reg_nr" class="col-md-4 control-label">Registration number</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="reg_nr" value="{{ $selectedCompany->registration_nr }}" placeholder="{{ $selectedCompany->registration_nr }}">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="address" class="col-md-4 control-label">Address</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="address" value="{{ $selectedCompany->address }}" placeholder="{{ $selectedCompany->address }}">
                          </div>
                      </div>


                      <div class="form-group">
                          <label for="representative" class="col-md-4 control-label">Representative role</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="representative" value="{{ $selectedCompany->representative }}" placeholder="{{ $selectedCompany->representative }}">
                          </div>
                      </div>

                      <input type="hidden" name="id" value="{{ $selectedCompany->id }}">

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              @include('redirect-buttons/back')
                              <button type="submit" class="btn btn-primary">
                                  Edit profile
                              </button>
                          </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
