@extends('layouts.app')
@section('content')

<div class="container">
    <div class="panel panel-default" style="position:fixed; right:0; left:0; margin-left:auto; margin-right:auto; width:60%; max-width:900px; min-width:550px">
        <div class="panel-heading">List of contracts</div>
            <div class="panel-body">
                @if (empty($contracts))
                    There are currently no active contract records in the system.<br><br>
                    @include('redirect-buttons/back')
                    <button type="button" class="btn btn-primary" onclick="location.href='{{ route('contracts.create.get') }}'">Go create contract</button>
                @else
                    <table frame="void" width="100%">
                      <div class="heading">
                          <tr><td align="center">Contract ID</td>
                          <td align="center">Date</td>
                          <td align="center">Person</td>
                          <td align="center">Amount</td>
                          <td align="center">Author work</td></tr>
                      </div>
                      @foreach ($contracts as $contract)
                        <tr><td align="center">{{ $contract['contract_id'] }}</td>
                        <td align="center">{{ $contract['date'] }}</td>
                        <td align="center">{{ $contract['person'] }}</td>
                        <td align="center">{{ $contract['amount'] }}</td>
                        <td align="center">
                            @if ($contract['author_work'] != "")
                              <div data-ot="{{ nl2br(htmlentities($contract['author_work'], ENT_QUOTES, 'UTF-8')) }}" class="glyphicon">&#xe118;</div>
                            @else
                              <div class="glyphicon">&#xe014;</div>
                            @endif
                        </td>
                        <td align="center">
                          <a style="color: inherit;" href="{{ URL::route('contracts.save.get', ['saveContract' => $contract['id']]) }}">
                            <span class="glyphicon glyphicon-save"></span>
                          </a>
                        </td>
                        <td align="center">
                          <a style="color: inherit;" href="{{ URL::route('contracts.delete.get', ['deleteContract' => $contract['id']]) }}" onclick="return confirm('Are you sure you want to delete the selected contract?');">
                            <span class="glyphicon glyphicon-trash"></span>
                          </a>
                        </td></tr>
                      @endforeach
                    </table><br><br>
                    @include('redirect-buttons/back')
                @endif
            </div>
    </div>
</div>
@endsection('content')
