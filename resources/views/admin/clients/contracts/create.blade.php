@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create a new contract</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{ route('contracts.create.post') }}">
                    {{ csrf_field() }}

                    @if (!empty($people))

                      <div class="form-group">
                          <label for="date" class="col-md-4 control-label">Date</label>
                          <div class="col-md-6">
                              <input type="date" class="form-control" name="date">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="person" class="col-md-4 control-label">Person</label>
                          <div class="col-md-6">
                              <select name="person" class="form-control">
                                  @foreach ($people as $person)
                                    <option value="{{ $person }}">{{ $person }}</option>
                                  @endforeach
                              </select>
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="amount" class="col-md-4 control-label">Amount</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="amount">
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="author_work" class="col-md-4 control-label">Author work</label>
                          <div class="col-md-6">
                              <textarea class="form-control" rows="3" name="author_work"></textarea>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              @include('redirect-buttons/back')
                              <button type="submit" class="btn btn-primary">
                                  Create contract
                              </button>
                          </div>
                      </div>

                  @else
                      You need to submit at least one person profile to create a contract.<br><br>
                      @include('redirect-buttons/back')
                      <button type="button" class="btn btn-primary" onclick="location.href='{{ route('people.create.get') }}'">Go create a profile</button>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
