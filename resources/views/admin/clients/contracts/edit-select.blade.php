@extends('layouts.app')
@section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">Edit contracts</div>
                  <div class="panel-body">
                        @if (empty($contracts))
                          There are currently no active contract records in the system.<br><br>
                          @include('redirect-buttons/back')
                          <button type="button" class="btn btn-primary" onclick="location.href='{{ route('contracts.create.get') }}'">Go create contract</button>
                        @else
                        <form role="form" method="GET" action="{{ route('contracts.edit.get') }}">
                          <div class="form-group">
                            Select contract:<br>
                              <select id="soflow-color" name="contractId">
                                @foreach ($contracts as $contract)
                                    <option value="{{ $contract }}">{{ $contract }}</option>
                                @endforeach
                              </select><br><br>
                            @include('redirect-buttons/back')
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </form>
                        @endif
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
