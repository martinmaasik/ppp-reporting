@if ($unfiltered == false)
    <div class="heading">
        <table style="float:left" frame="void" width="48%">
            <td align="center" height="40px"><i>Total</i></td>
        </table>

        <table style="float:right" frame="void" width="48%">
            <td align="center" height="40px"><i>Average per week</i></td>
        </table>
    </div>
    <table style="float:left" frame="void" width="48%">
        <td align="center">Name</td>
        <td align="center">Emails</td>
        <td align="center">Calls</td>
        <td align="center">Demos</td>
        <td align="center">Trials</td>
        <td align="center">Deals</td></tr>
        @foreach ($distinctUsers as $user)
          <td align="center">{{ $user }}</td>
          <td align="center">{{ $filteredData->where('user', $user)->pluck('emails')->sum() }}</td>
          <td align="center">{{ $filteredData->where('user', $user)->pluck('calls')->sum() }}</td>
          <td align="center">{{ $filteredData->where('user', $user)->pluck('demos')->sum() }}</td>
          <td align="center">{{ $filteredData->where('user', $user)->pluck('trials')->sum() }}</td>
          <td align="center">{{ $filteredData->where('user', $user)->pluck('deals')->sum() }}</td></tr>
        @endforeach
    </table>

    <table style="float:right" frame="void" width="48%">
        <td align="center">Name</td>
        <td align="center">Emails</td>
        <td align="center">Calls</td>
        <td align="center">Demos</td>
        <td align="center">Trials</td>
        <td align="center">Deals</td></tr>
        @foreach ($distinctUsers as $user)
          <td align="center">{{ $user }}</td>
          <td align="center">{{ (round($filteredData->where('user', $user)->pluck('emails')->average(),1)) }}</td>
          <td align="center">{{ (round($filteredData->where('user', $user)->pluck('calls')->average(),1)) }}</td>
          <td align="center">{{ (round($filteredData->where('user', $user)->pluck('demos')->average(),1)) }}</td>
          <td align="center">{{ (round($filteredData->where('user', $user)->pluck('trials')->average(),1)) }}</td>
          <td align="center">{{ (round($filteredData->where('user', $user)->pluck('deals')->average(),1)) }}</td></tr>
        @endforeach
    </table>

@endif

<div class="heading">
  <table frame="void" width="100%" height="40px">
    <td align="center" valign="bottom"><i>
      General overview
        @if ($unfiltered == true)
          <small>(ongoing week)</small>
        @endif
    </i></td>
  </table>
</div>

<table frame="void" width="100%">
  <td align="center">Name</td>
  <td align="center">Period</td>
  <td align="center">Emails</td>
  <td align="center">Calls</td>
  <td align="center">Demos</td>
  <td align="center">Trials</td>
  <td align="center">Deals</td>
  <td align="center">Details</td>
  <td align="center">Problems</td>
  <td align="center">Plans</td></tr>
  @foreach ($filteredData as $dataEntry)
    <td align="center">{{ $dataEntry->user }}</td>
    <td align="center">{{ $dataEntry->period }}</td>
    <td align="center">{{ $dataEntry->emails }}</td>
    <td align="center">{{ $dataEntry->calls}}</td>
    <td align="center">{{ $dataEntry->demos }}</td>
    <td align="center">{{ $dataEntry->trials }}</td>
    <td align="center">{{ $dataEntry->deals }}</td>
    <td align="center">
        @if ($dataEntry->notes != "")
          <div data-ot="{{ nl2br(htmlentities($dataEntry->notes, ENT_QUOTES, 'UTF-8')) }}" class="glyphicon">&#xe118;</div>
        @else
          <div class="glyphicon">&#xe014;</div>
        @endif
    </td>
    <td align="center">
        @if ($dataEntry->problems != "")
          <div data-ot="{{ nl2br(htmlentities($dataEntry->problems, ENT_QUOTES, 'UTF-8')) }}" class="glyphicon">&#xe118;</div>
        @else
          <div class="glyphicon">&#xe014;</div>
        @endif
    </td>
    <td align="center">
        @if ($dataEntry->next_week != "")
          <div data-ot="{{ nl2br(htmlentities($dataEntry->next_week, ENT_QUOTES, 'UTF-8')) }}" class="glyphicon">&#xe118;</div>
        @else
          <div class="glyphicon">&#xe014;</div>
        @endif
    </td>
    @if ($userStatus == 1)
        <td align="center">
            @if (isset($_GET['start_date']))
                <a style="color: inherit;" href="{{ URL::route('overview.delete', ['start_date' => $_GET['start_date'], 'end_date' => $_GET['end_date'], 'users' => $_GET['users'], 'deletedReport' => $dataEntry->id]) }}" onclick="return confirm('Are you sure you want to delete the selected PPP report?');">
            @else
                <a style="color: inherit;" href="{{ URL::route('overview.delete', ['deletedReport' => $dataEntry->id]) }}" onclick="return confirm('Are you sure you want to delete the selected PPP report?');">
            @endif
                <span class="glyphicon glyphicon-trash"></span>
                </a>
        </td>
    @endif
    </tr>
  @endforeach
</table><br><br>
