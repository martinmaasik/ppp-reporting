@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default" style="min-width:680px">
                <div class="panel-heading">Overview</div>
                    <div class="panel-body">
                        @if ($totalReportCount)
                            <form class="form-horizontal" method="POST" action="{{ route('overview.filter') }}">
                                {{ csrf_field() }}
                                    @include('overview/date-range')
                                    @include('overview/manager-selection')
                                    <button class="btn btn-primary">Filter</button>
                              </form>
                              <br>
                              @if ($errors->any())
                                  {{$errors->first()}} <br><br>
                              @elseif (count($filteredData))
                                  @include('overview/data-tables')
                              @elseif ($unfiltered == true)
                                  No PPP reports have been submitted this week.<br><br>
                              @endif
                          @else
                              No PPP reports have been submitted yet.<br><br>
                          @endif
                          @include('redirect-buttons/back')
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')
