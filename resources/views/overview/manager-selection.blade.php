<select class="selectpicker" multiple data-actions-box="true" name="users[]" title="Select users">
  @foreach ($usersWithReports as $userWithReports)
    <option value="{{ $userWithReports }}" {{ (collect(old('users'))->contains($userWithReports)) ? 'selected':'' }}>{{ $userWithReports }}</option>
  @endforeach
  @foreach ($usersWithoutReports as $userWithoutReports)
    <option disabled>{{ $userWithoutReports }}</option>
  @endforeach
</select>
