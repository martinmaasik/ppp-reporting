<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailConfig extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('mail_config', function (Blueprint $table) {
          $table->increments('id');
          $table->string('driver')->default('smtp')->nullable();
          $table->string('host')->default('smtp.gmail.com')->nullable();
          $table->string('port')->default('587')->nullable();
          $table->string('from_address')->default('noreply@maxtraffic.eu')->nullable();
          $table->string('from_name')->nullable();
          $table->string('encryption')->default('tls')->nullable();
          $table->string('username')->default('noreply@maxtraffic.eu')->nullable();
          $table->string('password')->default('Noreply!')->nullable();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
   public function down()
   {
       Schema::dropIfExists('mail_config');
   }
}
