<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('active')->default(1);
            $table->string('name')->nullable();
            $table->string('personal_code')->nullable();
            $table->string('address')->nullable();
            $table->string('passport_nr')->nullable();
            $table->string('passport_place')->nullable();
            $table->string('bank')->nullable();
            $table->string('swift', 20)->nullable();
            $table->string('iban', 40)->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
