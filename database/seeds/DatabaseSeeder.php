<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.admin',
            'password' => bcrypt('password'),
            'admin' => 1,
        ]);
        DB::table('mail_config')->insert(['id' => 1]); // editable email configuration settings
        DB::table('mail_config')->insert(['id' => 2]); // default email configuration settings
    }
}
