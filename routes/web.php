<?php

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

// MUTUAL ROUTES

Route::get('/PPP/overview', 'OverviewController@indexOverview')->name('overview.get');
Route::post('/PPP/overview', 'OverviewController@filter')->name('overview.filter');

// USER ROUTES

Route::get('/home', 'User\HomeController@index')->name('home');
Route::prefix('PPP')->group(function() {
    Route::get('submit', 'User\PPP\SubmitController@index')->name('PPP.submit.get');
    Route::post('submit', 'User\PPP\SubmitController@select')->name('PPP.submit.select');
    Route::post('submitted', 'User\PPP\SubmitController@submit')->name('PPP.submit');
    Route::get('view', 'User\PPP\ViewController@index')->name('PPP.view.get');
    Route::post('view', 'User\PPP\ViewController@view');
    Route::get('edit', 'User\PPP\EditController@index')->name('PPP.edit.get');
    Route::post('edit', 'User\PPP\EditController@select')->name('PPP.edit.select');
    Route::post('edited', 'User\PPP\EditController@submit')->name('PPP.edit');
});

// ADMIN ROUTES

Route::get('/PPP/overview/delete', 'OverviewController@delete')->name('overview.delete');

Route::middleware('admin')->prefix('admin')->group(function () {
    Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');
    Route::get('/configuration', 'Admin\Config\ConfigController@index')->name('config.get');
    Route::post('/configuration', 'Admin\Config\ConfigController@config')->name('config.post');
});

Route::middleware('admin')->prefix('admin/employees')->group(function () {
    Route::get('/create', 'Admin\Employees\CreateController@index')->name('create.get');
    Route::post('/create', 'Admin\Employees\CreateController@create')->name('create');
    Route::get('/edit/select', 'Admin\Employees\EditController@indexSelect')->name('edit.select.get');
    Route::post('/edit/select', 'Admin\Employees\EditController@Select')->name('edit.select.post');
    Route::get('/edit', 'Admin\Employees\EditController@indexEdit')->name('edit.get');
    Route::post('/edit', 'Admin\Employees\EditController@Edit')->name('edit.post');
});

Route::middleware('admin')->prefix('admin/companies')->group(function () {
    Route::get('/', 'Admin\Clients\CompanyController@index')->name('companies.menu.get');
    Route::get('/create', 'Admin\Clients\CompanyController@indexCreate')->name('companies.create.get');
    Route::post('/create', 'Admin\Clients\CompanyController@create')->name('companies.create.post');
    Route::get('/edit/select', 'Admin\Clients\CompanyController@indexSelect')->name('companies.edit.select.get');
    Route::post('/edit/select', 'Admin\Clients\CompanyController@Select')->name('companies.edit.select.post');
    Route::get('/edit', 'Admin\Clients\CompanyController@indexEdit')->name('companies.edit.get');
    Route::post('/edit', 'Admin\Clients\CompanyController@Edit')->name('companies.edit.post');
});

Route::middleware('admin')->prefix('admin/people')->group(function () {
    Route::get('/', 'Admin\Clients\PersonController@index')->name('people.menu.get');
    Route::get('/create', 'Admin\Clients\PersonController@indexCreate')->name('people.create.get');
    Route::post('/create', 'Admin\Clients\PersonController@create')->name('people.create.post');
    Route::get('/edit/select', 'Admin\Clients\PersonController@indexSelect')->name('people.edit.select.get');
    Route::post('/edit/select', 'Admin\Clients\PersonController@Select')->name('people.edit.select.post');
    Route::get('/edit', 'Admin\Clients\PersonController@indexEdit')->name('people.edit.get');
    Route::post('/edit', 'Admin\Clients\PersonController@Edit')->name('people.edit.post');
});

Route::middleware('admin')->prefix('admin/contracts')->group(function () {
    Route::get('/', 'Admin\Clients\ContractController@index')->name('contracts.menu.get');
    Route::get('/create', 'Admin\Clients\ContractController@indexCreate')->name('contracts.create.get');
    Route::post('/create', 'Admin\Clients\ContractController@Create')->name('contracts.create.post');
    Route::get('/edit/select', 'Admin\Clients\ContractController@indexSelect')->name('contracts.edit.select.get');
    Route::post('/edit/select', 'Admin\Clients\ContractController@Select')->name('contracts.edit.select.post');
    Route::get('/edit', 'Admin\Clients\ContractController@indexEdit')->name('contracts.edit.get');
    Route::post('/edit', 'Admin\Clients\ContractController@Edit')->name('contracts.edit.post');
    Route::get('/list', 'Admin\Clients\ContractController@indexList')->name('contracts.list.get');
    Route::get('/delete', 'Admin\Clients\ContractController@delete')->name('contracts.delete.get');
    Route::get('/save', 'Admin\Clients\ContractController@save')->name('contracts.save.get');
});
