<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($newReportData, $adminName)
    {
        $this->newReportData = $newReportData;
        $this->adminName = $adminName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.emails.newReport')
                    ->subject('New PPP report by ' . $this->newReportData['user'])
                    ->with(['newReportData' => $this->newReportData,
                            'adminName' => $this->adminName]);
    }
}
