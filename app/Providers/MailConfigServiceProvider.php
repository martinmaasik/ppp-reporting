<?php

namespace App\Providers;

use Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if (\Schema::hasTable('mail_config')) {
            $mail = DB::table('mail_config')->first();
            if ($mail) //checking if table is not empty
            {
                $config = array(
                    'driver'     => $mail->driver,
                    'host'       => $mail->host,
                    'port'       => $mail->port,
                    'from'       => array('address' => $mail->from_address, 'name' => $mail->from_name),
                    'encryption' => $mail->encryption,
                    'username'   => $mail->username,
                    'password'   => $mail->password,
                    'sendmail'   => '/usr/sbin/sendmail -bs',
                    'pretend'    => false,
                    'sendmail' => '/usr/sbin/sendmail -bs',
                    'markdown' => [
                        'theme' => 'default',
                        'paths' => [
                            resource_path('views/vendor/mail'),
                        ],
                    ],
                    'stream' => [
                        'ssl' => [
                          'allow_self_signed' => true,
                          'verify_peer' => false,
                          'verify_peer_name' => false,
                        ],
                    ],
                );
                Config::set('mail', $config);
            }
        }
    }
}
