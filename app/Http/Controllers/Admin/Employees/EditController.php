<?php

namespace App\Http\Controllers\Admin\Employees;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EditController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function indexSelect()
    {
        $allUserData = User::where('admin', "0")->where('active', "1")->get();
        return view('admin/employees/edit-select', ['allUserData' => $allUserData]);
    }

    public function select(Request $request)
    {
        if ($request->button == "Delete")
        {
          User::where('email', $request->user)->update(['active' => 0]);
          return redirect()->route('admin.dashboard')->with('message', 'User successfully deleted.');
        }
        else
        {
          $selectedUser = User::where('email', $request->user)->first();
          return response()->redirectToAction('Admin\Employees\EditController@indexEdit', ['id' => $selectedUser]);
        }
    }

    public function indexEdit() // extra get route here is needed so that return back withErrors would not redirect to the edit.select.get route
    {
        $selectedUser = User::where('id', $_GET['id'])->first();
        return view('admin/employees/edit', ['selectedUser' => $selectedUser]);
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255', // this validator does not require unique email
                'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails())
        {
              return back()->withInput()->withErrors($validator);
        }
        // so that admin could not change an user's email to an email address that is taken by another user already (basically another validator)
        elseif (User::where('email', $request->email)->count() == 1 && User::where('email', $request->email)->where('id', $request->id)->count() == 0)
        {
              return back()->withInput()->withErrors(['email' => ['The email has already been taken.']]);
        }
        else
        {
              User::where('id', $request->id)->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
              ]);
              return redirect()->route('admin.dashboard')->with('message', 'User data successfully updated.');
        }
    }
};
