<?php

namespace App\Http\Controllers\Admin\Employees;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CreateController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin/employees/create');
    }
    public function create(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails())
        {
              return back()->withInput()->withErrors($validator);
        }
        else
        {
              User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
              ]);
              return redirect()->route('admin.dashboard')->with('message', 'User successfully created.');
        }
    }
};
