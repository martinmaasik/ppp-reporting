<?php

namespace App\Http\Controllers\Admin\Clients;
use Illuminate\Http\Request;
use App\person;
use App\Http\Controllers\Controller;

class PersonController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin/clients/people/menu');
    }

    public function indexCreate()
    {
        return view('admin/clients/people/create');
    }

    public function create(Request $request)
    {
        person::create([
            'name' => $request->name,
            'personal_code' => $request->personal_code,
            'address' => $request->address,
            'passport_nr' => $request->passport_nr,
            'passport_place' => $request->passport_place,
            'iban' => $request->iban,
            'swift' => $request->swift,
            'bank' => $request->bank,
        ]);
        return redirect()->route('people.menu.get')->with('message', 'Person record successfully created.');
    }

    public function indexSelect()
    {
        $people = person::where('active', "1")->pluck('name')->toArray();
        return view('admin/clients/people/edit-select', ['people' => $people]);
    }

    public function select(Request $request)
    {
        if ($request->button == "Delete")
        {
          person::where('active', "1")->where('name', $request->name)->update(['active' => 0]);
          return redirect()->route('people.menu.get')->with('message', 'Person record successfully deleted.');
        }
        else
        {
          $selectedPerson = person::where('active', "1")->where('name', $request->name)->first();
          return response()->redirectToAction('Admin\Clients\PersonController@indexEdit', ['id' => $selectedPerson]);
        }
    }

    public function indexEdit() // so redirect back withErrors would work in case we decide to add validators
    {
        $selectedPerson = person::where('id', $_GET['id'])->first();
        return view('admin/clients/people/edit', ['selectedPerson' => $selectedPerson]);
    }

    public function edit(Request $request)
    {
        person::where('id', $request->id)->update([
              'name' => $request->name,
              'personal_code' => $request->personal_code,
              'address' => $request->address,
              'passport_nr' => $request->passport_nr,
              'passport_place' => $request->passport_place,
              'iban' => $request->iban,
              'bank' => $request->bank,
              'swift' => $request->swift,
        ]);
        return redirect()->route('people.menu.get')->with('message', 'Person record successfully updated.');
    }
};
