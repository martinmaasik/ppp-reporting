<?php

namespace App\Http\Controllers\Admin\Clients;
use Illuminate\Http\Request;
use App\company;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin/clients/companies/menu');
    }

    public function indexCreate()
    {
        return view('admin/clients/companies/create');
    }

    public function create(Request $request)
    {
        company::create([
              'name' => $request->name,
              'registration_nr' => $request->reg_nr,
              'address' => $request->address,
              'representative' => $request->representative,
        ]);
        return redirect()->route('companies.menu.get')->with('message', 'Company record successfully created.');
    }

    public function indexSelect()
    {
        $companies = company::where('active', "1")->pluck('name')->toArray();
        return view('admin/clients/companies/edit-select', ['companies' => $companies]);
    }

    public function select(Request $request)
    {
        if ($request->button == "Delete")
        {
          company::where('active', "1")->where('name', $request->name)->update(['active' => 0]);
          return redirect()->route('companies.menu.get')->with('message', 'Company record successfully deleted.');
        }
        else
        {
          $selectedCompany = company::where('active', "1")->where('name', $request->name)->first();
          return response()->redirectToAction('Admin\Clients\CompanyController@indexEdit', ['id' => $selectedCompany]);
        }
    }

    public function indexEdit() // so redirect back withErrors would work in case we decide to add validators
    {
        $selectedCompany = company::where('id', $_GET['id'])->first();
        return view('admin/clients/companies/edit', ['selectedCompany' => $selectedCompany]);
    }

    public function edit(Request $request)
    {
        company::where('id', $request->id)->update([
              'name' => $request->name,
              'registration_nr' => $request->reg_nr,
              'address' => $request->address,
              'representative' => $request->representative,
        ]);
        return redirect()->route('companies.menu.get')->with('message', 'Company record successfully updated.');
    }
};
