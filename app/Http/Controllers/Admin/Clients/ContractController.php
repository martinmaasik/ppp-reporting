<?php

namespace App\Http\Controllers\Admin\Clients;
use Illuminate\Http\Request;
use App\contract;
use App\person;
use PDF;
use App\Http\Controllers\Controller;

class ContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin/clients/contracts/menu');
    }

    public function indexCreate()
    {
        $people = person::where('active', "1")->pluck('name')->toArray();
        return view('admin/clients/contracts/create', ['people' => $people]);
    }

    public function create(Request $request)
    {
        $selectedPerson = person::where('active', "1")->where('name', $request->person)->value('id');
        $dateString = str_replace("-", "", $request->date);
        $contractId = $dateString . "-" . $selectedPerson;
        contract::create([
            'contract_id' => $contractId,
            'date' => $request->date,
            'author_work' => $request->author_work,
            'amount' => $request->amount,
            'person' => $request->person,
        ]);
        return redirect()->route('contracts.menu.get')->with('message', 'Contract record successfully created.');
    }

    public function indexSelect()
    {
        $contracts = contract::where('active', "1")->pluck('contract_id')->toArray();
        return view('admin/clients/contracts/edit-select', ['contracts' => $contracts]);
    }

    public function indexEdit() // so redirect back withErrors would work in case we decide to add validators
    {
        $people = person::where('active', "1")->pluck('name')->toArray();
        $selectedContract = contract::where('contract_id', $_GET['contractId'])->first();
        return view('admin/clients/contracts/edit', ['selectedContract' => $selectedContract, 'people' => $people]);
    }

    public function edit(Request $request)
    {
        $selectedPerson = person::where('active', "1")->where('name', $request->person)->value('id');
        $dateString = str_replace("-", "", $request->date);
        $contractId = $dateString . "-" . $selectedPerson;
        contract::where('id', $request->id)->update([
              'contract_id' => $contractId,
              'date' => $request->date,
              'author_work' => $request->author_work,
              'amount' => $request->amount,
              'person' => $request->person,
        ]);
        return redirect()->route('contracts.menu.get')->with('message', 'Contract record successfully updated.');
    }

    public function indexList()
    {
        $contracts = contract::where('active', "1")->get()->toArray();
        return view('admin/clients/contracts/list', ['contracts' => $contracts]);
    }

    public function delete()
    {
        contract::where('id', $_GET['deleteContract'])->update(['active' => 0]);
        return redirect()->route('contracts.list.get');
    }

    public function save()
    {
        $selectedContract = contract::where('id', $_GET['saveContract']);
        $html = '<h1> Contract ' . $selectedContract->value('contract_id') . '</h1><br><br><br>
                <table>
                    <tr><td style="width:100px">Date:</td><td style="width:300px">' . $selectedContract->value('date') . '</td></tr>
                    <tr><td>Person:</td><td>' . $selectedContract->value('person') . '</td></tr>
                    <tr><td>Amount:</td><td>' . $selectedContract->value('amount') . '</td></tr>
                    <tr><td>Author work:</td><td>' . $selectedContract->value('author_work') . '</td></tr>
                </table>';
        PDF::SetTitle('Contract ' . $selectedContract->value('contract_id'));
        PDF::SetMargins(30,40,30);
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output($selectedContract->value('contract_id') . '.pdf');
    }
};
