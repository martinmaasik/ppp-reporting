<?php

namespace App\Http\Controllers\Admin\Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Illuminate\Support\Facades\DB;
use App\mail_config;

class ConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $defaultEmailSettings = mail_config::where('id', 2)->first(); // for placeholders
        $oldEmailSettings = config('mail'); // same as ->where('id', 1)
        return view('admin/emails/configuration', ['old' => $oldEmailSettings, 'default' => $defaultEmailSettings]);
    }

    public function config(Request $request)
    {
        mail_config::where('id', 1)->update([
              'username' => $request->username,
              'password' => $request->password,
              'from_name' => $request->from_name,
              'from_address' => $request->from_address,
              'driver' => $request->driver,
              'host' => $request->host,
              'port' => $request->port,
              'encryption' => $request->encryption,
        ]);
        return redirect()->route('admin.dashboard')->with('message', 'E-mail configuration successfully updated.');
    }
};
