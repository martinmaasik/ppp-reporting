<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ppp_report;
use App\User;
use App\Http\Controllers\Controller;
use Auth;

class OverviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexOverview()
    {
        $userStatus = Auth::user()->admin;
        $activeUsers = User::where('active', "1")->pluck('name')->toArray();
        $totalReportCount = ppp_report::whereIn('user', $activeUsers)->where('active', "1")->count();
        $usersWithReports = ppp_report::distinct()->whereIn('user', $activeUsers)->where('active', "1")->pluck('user');
        $usersWithoutReports = User::where('admin', "0")->where('active', "1")->whereNotIn('name', $usersWithReports)->pluck('name');
        if (isset($_GET['start_date'])) // means that data has been filtered already
        {
          $query = ppp_report::whereIn('user', $_GET['users'])
                              ->where('period_start', '>=', $_GET['start_date'])
                              ->where('period_end', '<=', $_GET['end_date'])
                              ->where('active', "1");
          $unfiltered = false;
        }
        else
        {
          include(app_path().'/includes/dates.php');
          $query = ppp_report::where('period', $thisWeek)->whereIn('user', $activeUsers)->where('active', "1"); // by default shows ongoing week data
          $unfiltered = true;
        }
        return view('overview/overview', ['filteredData' => $query->get(),
                                          'distinctUsers' => $query->distinct()->pluck('user'), // this is for the data tables where each user has stats for average/sum
                                          'totalReportCount' => $totalReportCount, // so that filtering would not be possible if no reports have been submitted at all
                                          'unfiltered' => $unfiltered, // $unfiltered is being passed to retrieve view with correct tables
                                          'userStatus' => $userStatus, // to distinguish admins as they have exclusive right to delete reports
                                          'usersWithReports' => $usersWithReports,
                                          'usersWithoutReports' => $usersWithoutReports]); // they are visible in the user select dropdown but cannot be selected
    }

    public function filter(Request $request)
    {
        $data = $request->all();
        if (isset($request->start_date, $request->end_date, $request->users))
        {
            $selectedReports = ppp_report::where('active', "1")
                                          ->whereIn('user', $request->users)
                                          ->where('period_start', '>=', $request->start_date)
                                          ->where('period_end', '<=', $request->end_date)
                                          ->pluck('id')
                                          ->toArray();
            if (!empty($selectedReports))
            {
                return redirect()->route('overview.get', ['start_date'  => $request->start_date,
                                                          'end_date'    => $request->end_date,
                                                          'users'       => $request->users])->withInput();
            }
            else
            {
                return back()->withInput()->withErrors(['No PPP reports match filter criteria.']);
            }
        }
        else
        {
            return back()->withInput()->withErrors(['Please set all filter criteria.']);
        }
    }

    public function delete()
    {
        ppp_report::where('id', $_GET['deletedReport'])->update(['active' => 0]);
        if (isset($_GET['start_date'])) // means that data has been filtered already
        {
          return redirect()->route('overview.get', ['start_date'  => $_GET['start_date'],
                                                    'end_date'    => $_GET['end_date'],
                                                    'users'       => $_GET['users']])->withInput();
        }
        else
        {
          return redirect()->route('overview.get');
        }
    }

};
