<?php

namespace App\Http\Controllers\User\PPP;
use App\ppp_report;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewReport;

class SubmitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        include(app_path().'/includes/dates.php');
        $lastWeekSubmitted = ppp_report::where('user', Auth::user()->name)->where('active', 1)->where('period', $lastWeek)->count();
        $thisWeekSubmitted = ppp_report::where('user', Auth::user()->name)->where('active', 1)->where('period', $thisWeek)->count();
        return view('user/PPP/submit-select-period', ['lastWeekSubmitted' => $lastWeekSubmitted,
                                                      'thisWeekSubmitted' => $thisWeekSubmitted]);
    }

    public function select (Request $request)
    {
        $halfSubmittedData = $request->all();
        return view('user/PPP/submit', ['halfSubmittedData' => $halfSubmittedData]);
    }

    public function submit (Request $request)
    {
        $ppp_report = new ppp_report;
        $ppp_report->user = $request->user;
        $ppp_report->period = $request->period;
        $ppp_report->period_start = $request->period_start;
        $ppp_report->period_end = $request->period_end;
        $ppp_report->emails = $request->emails;
        $ppp_report->calls = $request->calls;
        $ppp_report->demos = $request->demos;
        $ppp_report->trials = $request->trials;
        $ppp_report->deals = $request->deals;
        $ppp_report->notes = $request->notes;
        $ppp_report->problems = $request->problems;
        $ppp_report->next_week = $request->plans;
        $ppp_report->remember_token = $request->_token;
        $ppp_report->save();
        $admins = User::where('admin', "1")->pluck('email')->toArray();
        $newReportData = $request->all();
        foreach ($admins as $admin)
        {
            $adminName = User::where('email', $admin)->value('name');
            try {
            Mail::to($admin)->send(new NewReport($newReportData, $adminName));
          } catch(\Exception $e){} // this will prevent users from seeing errors if email configuration settins/credentials are incorrectly set by admin
        }
        return redirect()->route('home')->with('message', 'PPP report successfully submitted.');
    }
}
