<?php

namespace App\Http\Controllers\User\PPP;
use Illuminate\Http\Request;
use App\ppp_report;
use App\Http\Controllers\Controller;
use Auth;

class ViewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $viewableReports = ppp_report::where('user', Auth::user()->name)->where('active', 1)->pluck('period');
        return view('user/PPP/view-select-period', ['viewableReports' => $viewableReports]);
    }
    public function view (Request $request)
    {
        $selectedViewableReport = ppp_report::where('period', $request->period)->where('user', Auth::user()->name)->where('active', 1)->first();
        return view('user/PPP/view', ['selectedViewableReport' => $selectedViewableReport]);
    }
}
