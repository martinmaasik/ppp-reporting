<?php

namespace App\Http\Controllers\User\PPP;
use Illuminate\Http\Request;
use App\ppp_report;
use App\Http\Controllers\Controller;
use Auth;

class EditController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        include(app_path().'/includes/dates.php');
        $editableReports = ppp_report::where('user', Auth::user()->name)->where('period', $lastWeek)->where('active', 1)
                                      ->orwhere('user', Auth::user()->name)->where('period', $thisWeek)->where('active', 1)
                                      ->pluck('period');
        return view('user/PPP/edit-select-period', ['editableReports' => $editableReports]);
    }
    public function select (Request $request)
    {
        $selectedEditableReport = ppp_report::where('period', $request->period)->where('active', 1)->where('user', Auth::user()->name)->first();
        return view('user/PPP/edit', ['selectedEditableReport' => $selectedEditableReport]);
    }
    public function submit (Request $request)
    {
      $ppp_report = ppp_report::where('user', $request->user)->where('period', $request->period)->where('active', 1)->first();
      $ppp_report->emails = $request->emails;
      $ppp_report->calls = $request->calls;
      $ppp_report->demos = $request->demos;
      $ppp_report->trials = $request->trials;
      $ppp_report->deals = $request->deals;
      $ppp_report->notes = $request->notes;
      $ppp_report->problems = $request->problems;
      $ppp_report->next_week = $request->plans;
      $ppp_report->remember_token = $request->_token;
      $ppp_report->save();
      return redirect()->route('home')->with('message', 'PPP report successfully edited.');
    }
}
