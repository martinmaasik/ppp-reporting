<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mail_config extends Model
{
    public $timestamps = false;

    protected $table = 'mail_config';

    protected $fillable = [
        'username', 'pessword', 'from_name', 'from_address', 'driver', 'host', 'port', 'encryption'
    ];
}
