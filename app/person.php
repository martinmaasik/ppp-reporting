<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class person extends Model
{
    protected $table = 'people';

    protected $fillable = [
        'name', 'personal_code', 'address', 'passport_nr', 'passport_place', 'iban', 'swift', 'bank'
    ];
}
